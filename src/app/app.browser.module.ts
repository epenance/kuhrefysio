
import { FrontpageComponent } from './frontpage/frontpage.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { PricePageComponent } from './price-page/price-page.component';
import { TreatmentPageComponent } from './treatment-page/treatment-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { ContentPageComponent } from './content-page/content-page.component';
import { PageContainerComponent } from './page-container/page-container.component';

import { MainNavigationComponent } from './main-navigation/main-navigation.component';
import { FrontpageSplashComponent } from './frontpage-splash/frontpage-splash.component';
import { PageSplashComponent } from './page-splash/page-splash.component';
import { TextSectionComponent } from './text-section/text-section.component';
import { GeneralInfoBoxesComponent } from './general-info-boxes/general-info-boxes.component';
import { ImageGridComponent } from './image-grid/image-grid.component';
import { FooterMapsComponent } from './footer-maps/footer-maps.component';
import { FooterComponent } from './footer/footer.component';
import { CtaImageComponent } from './cta-image/cta-image.component';
import { CtaBtnComponent } from './cta-btn/cta-btn.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { PriceListSectionComponent } from './price-list-section/price-list-section.component';
import { TreatmentListComponent } from './treatment-list/treatment-list.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { SingleTreatmentPageComponent } from './single-treatment-page/single-treatment-page.component';

import { GoogleMapsController } from './google-maps-controller.directive';
import { AgmCoreModule } from 'angular2-google-maps/core';

import { NO_SANITIZATION_PROVIDERS } from './no-sanitazion.service';
import { TopbarComponent } from './topbar/topbar.component';
import { MobileMenuComponent } from './mobile-menu/mobile-menu.component';
import { ImageNoGridComponent } from './image-no-grid/image-no-grid.component';

const mapsConfig = {
  apiKey: 'AIzaSyABgTlBPi4S_CUXI606RIVBXO4i6LPbWk8'
};

import { ReactiveFormsModule } from "@angular/forms";
import { WordpressService } from './wordpress.service';

import { AppComponent }  from './app.component';
import { appRouting } from './app.routes';
import { HttpModule } from '@angular/http';
/**
 * This file and `main.node.ts` are identical, at the moment(!)
 * By splitting these, you're able to create logic, imports, etc that are "Platform" specific.
 * If you want your code to be completely Universal and don't need that
 * You can also just have 1 file, that is imported into both
 * client.ts and server.ts
 */

import { NgModule } from '@angular/core';
import { UniversalModule } from 'angular2-universal';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Angulartics2Module, Angulartics2GoogleAnalytics } from 'angulartics2';
// import { RouterModule } from '@angular/router';
// import { appRoutes } from './app/app.routing';
import { Title } from '@angular/platform-browser';
import { SeoService } from './seo.service';

/**
 * Top-level NgModule "container"
 */
@NgModule({
  /** Root App Component */
  bootstrap: [ AppComponent ],
  /** Our Components */
  declarations: [
    AppComponent,
    PageContainerComponent,
    MainNavigationComponent,
    FrontpageComponent,
    FrontpageSplashComponent,
    PageSplashComponent,
    TextSectionComponent,
    GeneralInfoBoxesComponent,
    ImageGridComponent,
    FooterMapsComponent,
    FooterComponent,
    AboutPageComponent,
    GoogleMapsController,
    CtaImageComponent,
    CtaBtnComponent,
    EmployeeListComponent,
    PricePageComponent,
    PriceListSectionComponent,
    TreatmentPageComponent,
    TreatmentListComponent,
    SingleTreatmentPageComponent,
    ContactPageComponent,
    ContactFormComponent,
    ContentPageComponent,
    TopbarComponent,
    MobileMenuComponent,
    ImageNoGridComponent
   ],
  imports: [
    /**
     * NOTE: Needs to be your first import (!)
     * BrowserModule, HttpModule, and JsonpModule are included
     */
    UniversalModule,
    FormsModule,
    appRouting,
    ReactiveFormsModule,
    AgmCoreModule.forRoot(mapsConfig),
    /**
     * using routes
     */
    // RouterModule.forRoot(appRoutes)
    Angulartics2Module.forRoot([ Angulartics2GoogleAnalytics ]),
  ],
  providers: [
    WordpressService,
    NO_SANITIZATION_PROVIDERS,
    Title,
    SeoService
  ]
})
export class AppModule {

}
