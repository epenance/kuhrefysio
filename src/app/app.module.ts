import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from "@angular/forms";
import { WordpressService } from './wordpress.service';

import { AppComponent }  from './app.component';
import { appRouting } from './app.routes';
//import { MetaModule, MetaConfig, MetaService } from 'ng2-meta';

//Import page containers
import { FrontpageComponent } from './frontpage/frontpage.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { PricePageComponent } from './price-page/price-page.component';
import { TreatmentPageComponent } from './treatment-page/treatment-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { ContentPageComponent } from './content-page/content-page.component';
import { PageContainerComponent } from './page-container/page-container.component';

import { MainNavigationComponent } from './main-navigation/main-navigation.component';
import { FrontpageSplashComponent } from './frontpage-splash/frontpage-splash.component';
import { PageSplashComponent } from './page-splash/page-splash.component';
import { TextSectionComponent } from './text-section/text-section.component';
import { GeneralInfoBoxesComponent } from './general-info-boxes/general-info-boxes.component';
import { ImageGridComponent } from './image-grid/image-grid.component';
import { FooterMapsComponent } from './footer-maps/footer-maps.component';
import { FooterComponent } from './footer/footer.component';
import { CtaImageComponent } from './cta-image/cta-image.component';
import { CtaBtnComponent } from './cta-btn/cta-btn.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { PriceListSectionComponent } from './price-list-section/price-list-section.component';
import { TreatmentListComponent } from './treatment-list/treatment-list.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { SingleTreatmentPageComponent } from './single-treatment-page/single-treatment-page.component';

//import { GOOGLE_MAPS_PROVIDERS } from 'angular2-google-maps/core';
//import { GOOGLE_MAPS_DIRECTIVES, LazyMapsAPILoaderConfig, GoogleMapsAPIWrapper } from 'angular2-google-maps/core';
import { GoogleMapsController } from './google-maps-controller.directive';
import { AgmCoreModule } from 'angular2-google-maps/core';

import { NO_SANITIZATION_PROVIDERS } from './no-sanitazion.service';
import { TopbarComponent } from './topbar/topbar.component';
import { MobileMenuComponent } from './mobile-menu/mobile-menu.component';
import { ImageNoGridComponent } from './image-no-grid/image-no-grid.component';

/*const metaConfig: MetaConfig = {
  //Append a title suffix such as a site name to all titles
  //Defaults to false
  useTitleSuffix: true,
  defaults: {
    title: '',
    titleSuffix: ' | Kuhrefysio'
  }
};*/

const mapsConfig = {
  apiKey: 'AIzaSyABgTlBPi4S_CUXI606RIVBXO4i6LPbWk8'
};

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    appRouting,
    ReactiveFormsModule,
    AgmCoreModule.forRoot(mapsConfig)
    //MetaModule.forRoot(metaConfig)
  ],       // module dependencies
  declarations: [
     AppComponent,
     PageContainerComponent,
     MainNavigationComponent,
     FrontpageComponent,
     FrontpageSplashComponent,
     PageSplashComponent,
     TextSectionComponent,
     GeneralInfoBoxesComponent,
     ImageGridComponent,
     FooterMapsComponent,
     FooterComponent,
     AboutPageComponent,
     GoogleMapsController,
     CtaImageComponent,
     CtaBtnComponent,
     EmployeeListComponent,
     PricePageComponent,
     PriceListSectionComponent,
     TreatmentPageComponent,
     TreatmentListComponent,
     SingleTreatmentPageComponent,
     ContactPageComponent,
     ContactFormComponent,
     ContentPageComponent,
     TopbarComponent,
     MobileMenuComponent,
     ImageNoGridComponent
   ],
  bootstrap: [ AppComponent ],     // root component
  providers: [
    WordpressService,
    NO_SANITIZATION_PROVIDERS
  ]                    // services
})
export class AppModule { }
