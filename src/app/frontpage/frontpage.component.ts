import { Component, OnInit, Input } from '@angular/core';
import { WordpressService } from '../wordpress.service';


@Component({
  providers: [WordpressService],
  selector: 'frontpage',
  templateUrl: './frontpage.component.html',
  styleUrls: ['./frontpage.component.scss'],
})
export class FrontpageComponent implements OnInit {
  private settings: any;
  private phone: string;
  @Input() page: any;
  private imageGrid: any;

  constructor(private wordpress: WordpressService) { }

  ngOnInit() {
    this.wordpress.getSettings()
      .then(resolve => {
        this.settings = resolve.json().acf;
        this.phone = this.settings.phone.replace("+45 ", "");
      }).catch(err => {
        console.log(err);
        return {};
      });

    this.imageGrid = {
      image1: this.page.acf.image_grid_1.url,
      image2: this.page.acf.image_grid_2.url,
      image3: this.page.acf.image_grid_3.url,
      image4: this.page.acf.image_grid_4.url,
      image5: this.page.acf.image_grid_5.url,
      image6: this.page.acf.image_grid_6.url,
      image7: this.page.acf.image_grid_7.url,
    }
  }

}
