import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mobile-menu',
  templateUrl: './mobile-menu.component.html',
  styleUrls: ['./mobile-menu.component.scss']
})
export class MobileMenuComponent implements OnInit {
  @Input() menu: any;
  @Input() open: boolean;
  @Output() openChange = new EventEmitter();

  constructor() { }

  changeOpen(open) {
    this.open = !this.open;
    this.openChange.emit(this.open);
  }

  ngOnInit() {
  }

}
