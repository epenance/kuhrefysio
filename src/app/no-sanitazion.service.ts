import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class NoSanitizationService {
  sanitize(ctx: any, value: any): string {
    return value;
  }
}

export const NO_SANITIZATION_PROVIDERS: any[] = [
  {provide: DomSanitizer, useClass: NoSanitizationService}
];
