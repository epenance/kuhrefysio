/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { PageSplashComponent } from './page-splash.component';

describe('Component: PageSplash', () => {
  it('should create an instance', () => {
    let component = new PageSplashComponent();
    expect(component).toBeTruthy();
  });
});
