import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
//import { MetaConfig, MetaService } from 'ng2-meta';
import { isBrowser, isNode } from 'angular2-universal';
import { SeoService } from './seo.service';

@Injectable()
export class WordpressService {
  public test: any;
  public settings: any;
  private baseUrl = 'https://api.kuhrefysio.dk/wp-json/wp/v2';
  private rootUrl = 'https://api.kuhrefysio.dk/wp-json';

  constructor(private http: Http, private seoService: SeoService) {

   }

  getSettings() {
    if (!this.settings) {
      this.settings = this.http.get(`${this.rootUrl}/acf/v2/options`).toPromise()
      return this.settings;
    } else {
      return this.settings;
    }
  }

  getPageBySlug(slug: string) {
    return this.http.get(`${this.baseUrl}/pages/?filter[name]=${slug}`)
      .toPromise()
      .then(response => {
        let title = (typeof response.json()[0].title !== 'undefined' && response.json()[0].title !== null) ? response.json()[0].title.rendered : '';
        let image = (typeof response.json()[0].better_featured_image !== 'undefined' && response.json()[0].better_featured_image !== null) ? response.json()[0].better_featured_image.source_url : '';
        let description = (typeof response.json()[0].acf.seo_description !== 'undefined' && response.json()[0].acf.seo_description !== null) ? response.json()[0].acf.seo_description : '';
        let link = (typeof response.json()[0].link !== 'undefined' && response.json()[0].link !== null) ? response.json()[0].link.replace('https://api.kuhrefysio.dk', 'https://kuhrefysio.dk') : '';

        this.seoService.setMeta(title,
         description,
         [link],
         image,
         'Website',
         'Kuhrefyio');

        return response.json()[0];
      })
      .catch(err => {
        console.log(err);
        return {};
      });
  }

  getTreatmentBySlug(slug: string) {
    return this.http.get(`${this.baseUrl}/treatment/?filter[name]=${slug}`)
      .toPromise()
      .then(response => {
        let title = (typeof response.json()[0].title !== 'undefined' && response.json()[0].title !== null) ? response.json()[0].title.rendered : '';
        let image = (typeof response.json()[0].better_featured_image !== 'undefined' && response.json()[0].better_featured_image !== null) ? response.json()[0].better_featured_image.source_url : '';
        let description = (typeof response.json()[0].acf.seo_description !== 'undefined' && response.json()[0].acf.seo_description !== null) ? response.json()[0].acf.seo_description : '';
        let link = (typeof response.json()[0].link !== 'undefined' && response.json()[0].link !== null) ? response.json()[0].link.replace('https://api.kuhrefysio.dk', 'https://kuhrefysio.dk') : '';

        this.seoService.setMeta(title,
         description,
         [link],
         image,
         'Website',
         'Kuhrefyio');

        return response.json()[0];
      })
      .catch(err => {
        console.log(err);
        return {};
      });
  }

  getMenu(location) {
    return this.http.get(`${this.rootUrl}/wp-api-menus/v2/menu-locations/${location}`)
      .toPromise()
      .then(response => {
        let result = response.json();
        for(let i = 0; i < result.length; i++) {
          result[i].url = result[i].url.replace('https://api.kuhrefysio.dk', '');

          for(let u = 0; u < result[i].children.length; u++) {
            result[i].children[u].url = result[i].children[u].url.replace('https://api.kuhrefysio.dk', '');
          }
        }
        return result;
      })
      .catch(err => {
        console.error(err);
      })
  }
}
